///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalFactory.cpp
/// @version 1.0
///
/// Generates random Animal classes
///
/// @author Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date  03_20_21
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <random>
#include <time.h>
#include "animalFactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;

namespace animalfarm {

//srand(time(NULL));
random_device randomizer;
mt19937_64 RNG (randomizer());

Animal* AnimalFactory::getRandomAnimal() {
   //srand(time(NULL));
   uniform_int_distribution<> animalRNG(0,5);
   int animalNumber = animalRNG (RNG);
   Animal* newAnimal = NULL;

      //int i = rand() % 6;
   

   switch(animalNumber){
      case 0: newAnimal = new Cat (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 1: newAnimal = new Dog (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 2: newAnimal = new Nunu (Animal::getRandomBool(), RED, Animal::getRandomGender()); break;
      case 3: newAnimal = new Aku (Animal::getRandomWeight(15.0,100.0), SILVER, Animal::getRandomGender()); break;
      case 4: newAnimal = new Palila (Animal::getRandomName(), YELLOW, Animal::getRandomGender()); break;
      case 5: newAnimal = new Nene (Animal::getRandomName(), BROWN, Animal::getRandomGender()); break;
      
      }  
   return newAnimal;
}

} // namespace animalfarm
