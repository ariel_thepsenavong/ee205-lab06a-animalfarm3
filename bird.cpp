///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo 03_05_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream> 

#include "bird.hpp"

using namespace std;

namespace animalfarm {
	
void Bird::printInfo() {
	Animal::printInfo();
	cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
	cout << "   Is Migratory = [" << std::boolalpha <<isMigratory << "]" << endl;
}


const string Bird::speak() {
	return string( "Tweet" );
}

} // namespace animalfarm
