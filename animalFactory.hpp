///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalFactory.hpp
/// @version 1.0
///
/// Generates random Animal classes
///
/// @author Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03_21_21
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <time.h>
#include "animal.hpp"

namespace animalfarm {
	
class AnimalFactory {
public:
   static Animal* getRandomAnimal();
};

} // namespace animalfarm
