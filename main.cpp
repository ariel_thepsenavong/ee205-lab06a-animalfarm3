///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @03_05_2021
///////////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <iostream>
#include <array>
#include <random>
#include <memory>
#include <ctime>
#include <list>
#include "animal.hpp"
#include "animalFactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 3" << endl;
	array<Animal*, 30> animalArray;
	animalArray.fill( NULL );
	
  // srand(time(NULL));
	for( auto i = 0 ; i < 25 ; i++ ) {
		animalArray[i] = AnimalFactory::getRandomAnimal();
   }
	
	cout << endl;
	cout << "Array of Animals" << endl;
	cout << "  Is it empty: " << boolalpha << animalArray.empty() << endl;
	cout << "  Number of elements: " << animalArray.size() << endl;
	cout << "  Max size: " << animalArray.max_size() << endl;

	for( Animal* animal : animalArray ) {
		if( animal != NULL )
			cout << animal->speak() << endl;
	}

	for( auto i = 0 ; i < animalArray.size() ; i++ ) {
		if( animalArray[i] != NULL ) {
			delete animalArray[i];
			animalArray[i] = NULL;
		}
	}

  list<Animal*> animalList;

	for( auto i = 0 ; i < 25 ; i++ ) {
		animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
	}
	
	cout << endl;
	cout << "List of Animals" << endl;
	cout << "  Is it empty: " << boolalpha << animalList.empty() << endl;
	cout << "  Number of elements: " << animalList.size() << endl;
	cout << "  Max size: " << animalList.max_size() << endl;

	for( Animal* animal : animalList ) {
		cout << animal->speak() << endl;
	}

	for( auto iterator = animalList.begin() ; iterator != animalList.end() ; ) {
		delete *iterator;
		iterator = animalList.erase( iterator );
	}
	
	return 0;
}

