//#ifndef TESTMAIN_H
//#define TESTMAIN_H
//TEST.hpp
#pragma once
using namespace std;
//using namespace animalfarm;

#include <string>
//#include "animal.hpp"

namespace animalfarm{

class Animal{
public:
   enum Gender{MALE, FEMALE, UNKNOWN_GENDER};
   enum Color{RED, BLACK, WHITE, SILVER, YELLOW, BROWN, UNKNOWN_COLOR};
   static const string getRandomName();
   static const Gender getRandomGender();
   static const Color getRandomColor();
   static const float getRandomWeight(const float from, const float to);
   static const bool getRandomBool();
};

class AnimalFactory{
public:
   static Animal* getRandomAnimal();

}; 

}
//#endif
