//TEST.cpp

#include <iostream>
#include <random>
#include <string>
#include "testmain.hpp"
#include <array>
//#include "animal.hpp"

namespace animalfarm{
const Animal::Gender Animal::getRandomGender(){
   srand(time(NULL));
   Gender gender = Gender(rand()%3);

   switch(gender){
      case MALE: cout<<"Male"<<endl; break;
      case FEMALE: cout<<"Female"<<endl; break;
      case UNKNOWN_GENDER: cout<<"Unknown"<<endl; break;
   } 

   return UNKNOWN_GENDER;
};

const Animal::Color Animal::getRandomColor(){
   srand(time(NULL));
   Color color = Color(rand()%6);

   switch(color){
      case RED: cout << "Red" <<endl; break;
      case BLACK: cout << "Black" <<endl ; break;
     case WHITE: cout << "White" <<endl; break;
     case SILVER: cout << "Silver" <<endl; break;
     case YELLOW: cout << "Yellow" <<endl; break;
      case BROWN: cout << "Brown" <<endl; break;
   } 

   return UNKNOWN_COLOR;
};

const bool Animal::getRandomBool(){
   srand(time(NULL));
   bool pick = rand()%2;

   switch(pick){
      case 0: cout << "true" <<endl; break;
      case 1: cout << "false" <<endl ; break;
     
   } 

   return true;
};

const float Animal::getRandomWeight(const float from, const float to){
   srand(time(NULL));
   const float range = to - from;
   float random = range * ((((float) rand()) / (float) RAND_MAX)) + from;
   cout << random << endl;
   return random;


   
}

const string Animal::getRandomName(){
   char randomName[9];
   srand(time(NULL));
   int length = rand() % 6 + 4;  //random length from 4-9
   char upperC = 65 + rand()%26;
   randomName[0] = upperC;

   for(int i = 1; i< length; i++){
      randomName[i] = 97 +rand()%26;
   }

   cout << randomName << endl;
   return "";
};

/*Animal* AnimalFactory::getRandomAnimal(){
   Animal* newAnimal = NULL;
   int i = rand() % 6;

   switch(i){
      case 0: newAnimal = new Cat (RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER);
      case 1: newAnimal = new Dog (RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER);
      case 2: newAnimal = new Nunu (RANDOM_BOOL, RED, RANDOM_GENDER);
      case 3: newAnimal = new Aku (RANDOM_WEIGHT, SILVER, RANDOM_GENDER);
      case 4: newAnimal = new Palila (RANDOM_NAME, YELLOW, RANDOM_GENDER);
      case 5: newAnimal = new Nene (RANDOM_NAME, BROWN, RANDOM_GENDER);
   }

   return newAnimal; */
/*
   for(int i = 0; i<25; i++){
      Animal* a = AnimalFactory::getRandomAnimal();
      cout << a.speak() << endl;*/

  // }
}; 


}


int main(){
   cout << "Is this working?" << endl;
   animalfarm::Animal animal;
   animal.getRandomName();
   animal.getRandomGender();
   animal.getRandomColor();
   animal.getRandomBool();
   animal.getRandomWeight(30, 100);
   /*std::array<animalfarm::Animal*, 30> animalArray;
   animalArray.fill(NULL);
   for(int i = 0; i<25; i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }*/
   return 0; 
}
